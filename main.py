from read_data import read_rules, read_data
from functions import process_data_table, process_missing_data, add_cutpoints,check_conditional_ok,\
    classify_case_complete, print_data, get_support_complete, classify_case_partial, get_support_partial


# rf_name = "car.r.p"+".txt"
# df_name = "car"+".txt"

# Main
process_flag = True
while process_flag:
    # 1. Reading files
    flag = True
    while flag:
        rf_name = input("Please input the name of the input rule file, eg:\"car.r.p.txt\"\n")
        try:
            rf = open(rf_name, 'r')
            flag = False
        except:
            print("The rule file can not be open.")
            continue
    rules = read_rules(rf)
    rf.close()

    flag = True
    while flag:
        df_name = input("Please input the name of the input data file, eg:\"car.txt\"\n")
        try:
            df = open(df_name, 'r')
            flag = False
        except:
            print("The data file can not be open.")
            continue
    data, attributes_name, decision_name = read_data(df)
    df.close()

    # print(attributes_name)
    # print(decision_name)
    # print(len(data))

    # 2. Processing data
    attributes_dict, decision_dict, decision_list = process_data_table(data, attributes_name)
    # Extra processing, numerical processing should be first
    add_cutpoints(rules, attributes_dict)
    process_missing_data(attributes_dict)

    # print(attributes_dict)
    # print(decision_dict)

    conditional_ok = True
    total_conditions = 0
    for i in rules:
        total_conditions += len(i.conditions)
        i.get_complete_matches(attributes_dict)  # Compute the complete matching cases
        i.get_partial_matches(attributes_dict)  # Compute the partial matching cases
        i.get_complete_corrects(decision_dict)
        # print(i)

    # 3. Ask questions
    # Four questions for the choice of the best decision.
    choice = input("Do you want to use matching factor (enter 'y' for yes)?")
    if choice == 'y':
        matching_factor = True
    else:
        matching_factor = False
    # matching_factor = True

    choice = input("You wish whether strength (enter 's') "
                       "or conditional probability (enter 'p') to be used as the strength factor?")
    if choice == 'p':
        if check_conditional_ok(rules):
            strength_factor = False
        else:
            print('The conditional probability option cannot be executed.There are rules with no matching cases.')
            strength_factor = True
    else:
        strength_factor = True
    # strength_factor = True

    choice = input("Do you want to use the factor associated with specialty? (enter 'y' for yes)")
    if choice == 'y':
        specialty = True
    else:
        specialty = False
    # specialty = True

    choice = input("Do you want to use the support from other rules? (enter 'y' for yes)")
    if choice == 'y':
        other_support = True
    else:
        other_support = False

    # Whether Concept statics
    choice = input("Do you want to print the concept statistics? (enter 'y' for yes)")
    if choice == 'y':
        concept_statics = True
    else:
        concept_statics = False

    # Whether Cases associated with concepts?
    choice = input("Do you want to know how cases associated with concepts were classified? (enter 'y' for yes)")
    if choice == 'y':
        case_concept_statistic = True
    else:
        case_concept_statistic = False

    # 4. Classifying based on concepts
    concept_result = {}

    # Calculation voting factors
    for item in decision_dict.keys():
        concept_result[item] = [[], [], [], [], []]
        # 0: complete correctly classified
        # 1: complete incorrectly classified
        # 2: partial correctly classified
        # 3: partial incorrectly classified
        # 4: not classified

    # Complete classification
    for concept, concept_cases in decision_dict.items():
        for case in concept_cases:
            matching_rules = classify_case_complete(case, rules)  # Return the list of rules
            if len(matching_rules) == 0:  # Not classified
                concept_result[concept][4].append(case)
            elif len(matching_rules) == 1:    # Classified without conflict
                if matching_rules[0].decision == concept:
                    concept_result[concept][0].append(case)
                else:
                    concept_result[concept][1].append(case)
            else:   # Matching rules >= 2
                voting = get_support_complete(matching_rules, strength_factor, specialty, other_support)
                if voting == concept:
                    concept_result[concept][0].append(case)
                else:
                    concept_result[concept][1].append(case)
            # print(case, matching_rules)
    # print(concept_result)

    # Partial classification
    for concept, clist in concept_result.items():
        for case in clist[4][:]:
            matching_rules = classify_case_partial(case, rules)
            if len(matching_rules) == 0:  # Not classified
                continue
            else:
                concept_result[concept][4].remove(case)
                if len(matching_rules) == 1:  # Classified without conflict
                    if matching_rules[0].decision == concept:
                        concept_result[concept][2].append(case)
                    else:
                        concept_result[concept][3].append(case)
                else:   # Matching rules >= 2
                    voting = get_support_partial(matching_rules, matching_factor, strength_factor, specialty, other_support, case)
                    if voting == concept:
                        concept_result[concept][2].append(case)
                    else:
                        concept_result[concept][3].append(case)
    # print(concept_result)

    # Collect total info
    correctly_classified_c = 0
    correctly_classified_p = 0
    incorrectly_classified_c = 0
    incorrectly_classified_p = 0
    not_classified = 0
    for concept_cases in concept_result.values():
        correctly_classified_c += len(concept_cases[0])
        correctly_classified_p += len(concept_cases[2])
        incorrectly_classified_c += len(concept_cases[1])
        incorrectly_classified_p += len(concept_cases[3])
        not_classified += len(concept_cases[4])
    correctly_classified = correctly_classified_c + correctly_classified_p
    incorrectly_classified = incorrectly_classified_c + incorrectly_classified_p

    # 5. Report "General statistics"
    print('\n******      General statistics      ******')
    print("The report was created from: <" + rf_name + "> and from: <" + df_name + ">.")
    print('The total number of cases:\t' + str(len(data)))
    print('The total number of attributes:\t' + str(len(attributes_name)))
    print('The total number of rules:\t' + str(len(rules)))
    print('The total number of conditions:\t' + str(total_conditions))
    print('The total number of cases that are not classified:\t' + str(not_classified))
    print('\t\tPARTIAL MATCHING:')
    print('\tThe total number of cases that are incorrectly classified:\t' + str(incorrectly_classified_p))
    print('\tThe total number of cases that are correctly classified:\t' + str(correctly_classified_p))
    print('\t\tCOMPLETE MATCHING:')
    print('\tThe total number of cases that are incorrectly classified:\t' + str(incorrectly_classified_c))
    print('\tThe total number of cases that are correctly classified:\t' + str(correctly_classified_c))
    print('\t\tPARTIAL AND COMPLETE MATCHING:')
    print('The total number of cases that are not classified or incorrectly classified:\t'
          + str(incorrectly_classified + not_classified))
    temp_result = float(incorrectly_classified + not_classified) / float(len(decision_list))
    print('Error rate: ' + str(round(temp_result, 2)))

    # 6. Report concept statistic
    if concept_statics:
        print('\n\n******      Concept statistics      ******')
        for concept,concept_cases in concept_result.items():
            print('Concept (' + decision_name + ', ' + concept+'):')
            print('The total number of cases that are not classified:\t' + str(len(concept_cases[4])))

            print('\t\tPARTIAL MATCHING:')
            print('\tThe total number of cases that are incorrectly classified:\t' + str(len(concept_cases[3])))
            print('\tThe total number of cases that are correctly classified:\t' + str(len(concept_cases[2])))

            print('\n\t\tCOMPLETE MATCHING:')
            print('\tThe total number of cases that are incorrectly classified:\t' + str(len(concept_cases[1])))
            print('\tThe total number of cases that are correctly classified:\t' + str(len(concept_cases[0])))
            print('The total number of cases in the concept:' + str(len(decision_dict[concept])))
            print('')

    # 7. Report case-concept statistic
    if case_concept_statistic:
        print('\n\n******      Cases statistics      ******')
        for concept, concept_cases in concept_result.items():
            print('Concept (' + decision_name + ', ' + concept + '):')
            print('List of cases that are not classified:\t'+ str(len(concept_cases[4])))
            for case in concept_cases[4]:
                print(print_data(data[case-1]))

            print('\n\t\tPARTIAL MATCHING:')
            print('\tList of cases that are incorrectly classified:' + str(len(concept_cases[3])))
            for case in concept_cases[3]:
                print(print_data(data[case-1]))
            print('\tList of cases that are correctly classified:' + str(len(concept_cases[2])))
            for case in concept_cases[2]:
                print(print_data(data[case-1]))

            print('\n\t\tCOMPLETE MATCHING:')
            print('\tList of cases that are incorrectly classified:' + str(len(concept_cases[1])))
            for case in concept_cases[1]:
                print(print_data(data[case-1]))
            print('\n\tList of cases that are correctly classified:' + str(len(concept_cases[0])))
            for case in concept_cases[0]:
                print(print_data(data[case-1]))
            print('')

    # 8. Finished
    over_again = input("Do you wish to exit the program "
                       "or start the program all over again (enter 'y')?")

    if over_again != 'y':
        process_flag = False
    # process_flag = False
