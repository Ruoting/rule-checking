from rule import Rule
import re


def read_data(df):
    datas = []
    attributesname = []
    decisionname = ""
    try:
        for line in df.readlines():
            line_list = line.split()
            if line_list:
                if line_list[0] == "<":
                    continue
                if line_list[0] == "[":
                    attributesname = line_list[1:len(line_list) - 2]
                    decisionname = line_list[-2]
                    continue
                if line_list[0][0] == '!':  # Comment start
                    continue
                datas.append(line_list)
        return datas, attributesname, decisionname
    except:
        print("Error occurs in reading data.")
        exit()


def read_rules(rf):
    rules = []
    conditions = {}
    specialty = -1
    strength = -1
    match_cases = -1

    # Improve tip: We can read the whole file instead of reading by lines.
    # And remove comments by re.

    para_p = re.compile('(?P<spe>\d+)[ ]*,[ ]*(?P<str>\d+)[ ]*,[ ]*(?P<cases>\d+)')
    condition_p = re.compile('\((?P<attribute>\S+)[ ]*,[ ]*(?P<value>\S+)\)')
    symbol_p = re.compile('[\^&]+')
    arrow_p = re.compile('-+\>')
    decision_p = re.compile('.*\((?P<decision>\S+)[ ]*,[ ]*(?P<value>\S+)\)')
    text_p = re.compile('\S')

    flag = 3
    # 0 Wait for reading an av pair
    # 1 Finish reading an av pair
    # 2 Finish reading conditions, wait for reading decision
    # 3 Finish reading a Rule
    try:
        for line in rf.readlines():
            # line_list = line.split()
            # for item in line_list:
            if line[0] == '!':  # Comment start
                continue

            start = 0
            while start < len(line):
                #Content
                if flag == 3:
                    para_match = para_p.match(line[start:])
                    if para_match:
                        linebits = para_match.groupdict()
                        specialty = linebits['spe']
                        strength = linebits['str']
                        match_cases = linebits['cases']
                        flag = 0
                        # print(specialty,strength,match_cases)
                        start += para_match.end()
                        continue

                if flag == 0:
                    condition_match = condition_p.match(line[start:])
                    if condition_match:
                        linebits = condition_match.groupdict()
                        # print linebits
                        # for k, v in linebits.items():
                        #     print k + ": " + v
                        conditions[linebits['attribute']] = linebits['value']
                        flag = 1
                        # print(linebits)
                        start += condition_match.end()
                        continue

                if flag == 1:
                    symbol_match = symbol_p.match(line[start:])
                    arrow_match = arrow_p.match(line[start:])
                    if symbol_match:
                        flag = 0
                        start += symbol_match.end()
                        # print('symbol &', flag)
                        continue
                    elif arrow_match:
                        flag = 2
                        start += arrow_match.end()
                        # print('symbol ->',flag)
                        continue

                if flag == 2:
                    decision_match = decision_p.match(line[start:])
                    if decision_match:
                        linebits = decision_match.groupdict()
                        decision = linebits['value']
                        flag = 3
                        start += decision_match.end()
                        a_rule = Rule(specialty,strength,match_cases,conditions,decision)
                        rules.append(a_rule)
                        conditions = {}
                        # print('decision',decision)
                        continue

                text_match = text_p.match(line[start])
                start += 1
                if text_match:
                    print('The rule file is not in the proper format.')
                    exit()

        if flag != 3 or len(rules) == 0:
            print('The rule file is not in the proper format.')
            exit()
        return rules
    except:
        print("Error occurs in reading rules.")
        exit()

