import re


# Get intersection of lists
def combine_list(candidates):
    if len(candidates):
        result = candidates[0]
        for num in range(1, len(candidates)):
            result = [i for i in result if i in candidates[num]]
        return result
    else:
        return []


def process_data_table(data, attributes_name):
    # Initialization
    attributes_dict = {}
    decision_dict = {}
    decision_list = []
    num_attributes = len(attributes_name)

    for item in attributes_name:
        attributes_dict[item] = {}

    # Read data
    num = 1 # number of cases, starts from 1
    for line in data:
        # In case there is line is just a '\n'
        if len(line) < num_attributes:
            break
        for i in range(len(attributes_name)):
            if line[i] in attributes_dict[attributes_name[i]]:
                attributes_dict[attributes_name[i]][line[i]].append(num)
            else:
                attributes_dict[attributes_name[i]][line[i]] = [num]
        if line[i+1] in decision_dict:
            decision_dict[line[i+1]].append(num)
        else:
            decision_dict[line[i+1]] = [num]
        decision_list.append(line[-1])
        num += 1

    return attributes_dict, decision_dict, decision_list


def process_missing_data(attributes_dict):  # Assign cases with '*' or '-' to all
    for attribute in attributes_dict.values():
        temp1 = []
        temp2 = []
        if '*' in attribute:
            temp1 = attribute['*']
            attribute.pop('*')
        if '-' in attribute:
            temp2 = attribute['-']
            attribute.pop('-')
        for alist in attribute.values():
            alist.extend(temp1)
        for blist in attribute.values():
            blist.extend(temp2)


def add_cutpoints(rules, attributes_dict):

    for attribute, value in attributes_dict.items():
        new_numerical_dict = {}
        # Check if attribute is numerical: go through data file
        is_numerical = True
        for i in value.keys():
            if i == '*' or i == '-' or i == '?':
                new_numerical_dict[i] = value[i]
                continue
            if not is_number(i):
                is_numerical = False
                break

        if is_numerical:
            # Get all cutpoints pair: from conditions in rules
            cut_points = {}
            for rule in rules[:]:
                if attribute in rule.conditions:
                    cut_points[rule.conditions[attribute]] = []

            number_p = re.compile('(?P<lower>\S+)\.\.(?P<upper>\S+)')
            no_error = True

            # Merge blocks
            for new_block in cut_points.keys():
                new_numerical_dict[new_block] = []
                number_match = number_p.match(new_block)
                if number_match:
                    linebits = number_match.groupdict()
                else:
                    print("Error in reading cutpoints from rule's condition")
                    no_error = False
                    break
                lower = float(linebits['lower'])
                upper = float(linebits['upper'])

                for j in value.keys():
                    if j == '*' or j == '-' or j == '?':
                        continue
                    if lower <= float(j) <= upper:
                        new_numerical_dict[new_block].extend(value[j])
            if no_error:
                attributes_dict[attribute] = new_numerical_dict
                print("There is a numerical attribute: "+attribute)
                print(attributes_dict[attribute].keys())


# Using list
def get_decision(decision_list, case):
    return decision_list[case-1]


def check_conditional_ok(rules):
    for rule in rules:
        if rule.match_cases == 0:
            return False
    return True


def classify_case_complete(case, rule_set):  # Return the rule that covered this case
    support_rules = []
    for rule in rule_set[:]:
        if case in rule.complete_matches:
            support_rules.append(rule)
    return support_rules


def classify_case_partial(case, rule_set):  # Return the rule that partially covered this case
    support_rules = []
    for rule in rule_set[:]:
        if case in rule.partial_matches:
            support_rules.append(rule)
    return support_rules


def get_support_complete(matching_rules, if_strength_factor, if_specialty, other_support):
    if other_support:
        support_score = {}
        for rule in matching_rules[:]:
            if rule.decision not in support_score:
                support_score[rule.decision] = 0

    max_value = -1
    max_concept = ''

    for rule in matching_rules[:]:
        if if_strength_factor:
            strength_factor = rule.strength
        else:
            strength_factor = rule.conditional_p
        if if_specialty:
            specialty_factor = rule.specialty
        else:
            specialty_factor = 1
        support = specialty_factor * strength_factor

        if other_support:
            support_score[rule.decision] += support
        else:
            if support > max_value:
                max_concept = rule.decision
                max_value = support

    if other_support:
        for concept, score in support_score.items():
            if score > max_value:
                max_concept = concept
                max_value = score
    return max_concept


def get_support_partial(matching_rules, if_matching_factor ,if_strength_factor, if_specialty, other_support, case):
    if other_support:
        support_score = {}
        for rule in matching_rules[:]:
            if rule.decision not in support_score:
                support_score[rule.decision] = 0

    max_value = -1
    max_concept = ''

    for rule in matching_rules[:]:
        if if_strength_factor:
            strength_factor = rule.strength
        else:
            strength_factor = rule.conditional_p
        if if_specialty:
            specialty_factor = rule.specialty
        else:
            specialty_factor = 1
        if if_matching_factor:
            matching_factor = rule.partial_matches[case]
        else:
            matching_factor = 1
        support = specialty_factor * strength_factor * matching_factor

        if other_support:
            support_score[rule.decision] += support
        else:
            if support > max_value:
                max_concept = rule.decision
                max_value = support

    if other_support:
        for concept, score in support_score.items():
            if score > max_value:
                max_concept = concept
                max_value = score
    return max_concept


def print_data(case):
    string = '\t\t'+case[0]
    for i in case[1:]:
        string += ',  '+i
    return string


# Judge if it is a numerical attribute
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False


