# README #
* Author info
KUID r540z390 2872821
Ruoting Zheng

### What is this repository for? ###
For KU EECS 839 project.
* Quick summary
This is a rule checking system for data classification.
* Version 
Project final submit (modified)
# 4/10/2018 Python 2 version
4/16/2018 Python 3 version

### How do I get set up? ###
* How to run the program
1. cd into the code directory (please put all the code files in the same folder)
2. Run main.py		"python main.py"
3. Get your data file and rule file ready, input the path of files in the terminal, and run the program following the instructions.

The program is using "raw_input" which is only supported by Python 2, since some of the departmental computers do not have Python 3 environment. 
Please run the program using Python 3.

* About the input files:
The program accept two input files:
1. The file containning rules.
2. The file containning data in the LERS format.

* About my code
This program is based on Python 3.5. Start it with python.
The code files include:
main.py 		# Main process
read_data.py		# Data input and processing
functions.py		# Supporting functions
rule.py			# Rule class definition and functions


### Contribution guidelines ###
Ruoting, started from 3/23/2018
* Tests
The car, austr-aca, m-iris dataset provided by professor are in the repository.

