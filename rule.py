from functions import combine_list


class Rule:

    def __init__(self, a1, a2, a3, avpair, decision):
        # Components that get from reading data
        self.specialty = int(a1)
        self.strength = int(a2)
        self.match_cases = int(a3)
        self.conditions = avpair
        self.decision = decision
        if self.match_cases != 0:
            self.conditional_p = float(self.strength)/float(self.match_cases)

        # Components need calculation
        self.complete_matches = []  # list of cases
        self.complete_corrects = [] # list of cases
        self.partial_matches = {}  # case:matching factor

    def __repr__(self):
        return 'Rule:' + str(self.specialty) + str(self.strength) + str(self.match_cases) \
               + str(self.conditions) + str(self.decision) + str(self.complete_matches) \
        + str(self.conditional_p)

    def get_complete_matches(self, attributes_dict):   # use dict
        candidates_list = []
        for attribute, value in self.conditions.items():
            if value not in attributes_dict[attribute]:
                attributes_dict[attribute][value] = []
            candidates_list.append(attributes_dict[attribute][value])

        self.complete_matches = combine_list(candidates_list)

    def get_partial_matches(self, attributes_dict):   # use dict
        for attribute, value in self.conditions.items():
            for case in attributes_dict[attribute][value]:
                if case in self.partial_matches:
                    self.partial_matches[case] += 1
                else:
                    self.partial_matches[case] = 1
        for case, factor in self.partial_matches.items():
            self.partial_matches[case] = float(factor)/float(self.specialty)
        # print(self.partial_matches)

    def get_complete_corrects(self, decision_dict):
        candidates_list = [self.complete_matches, decision_dict[self.decision]]
        self.complete_corrects = combine_list(candidates_list)




